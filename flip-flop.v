module fewlog (s, r, q1, q2);

input        s, r;
output       q1, q2;
//reg          o;

assign q1 = ~(s & ~r);
assign q2 = ~(r & ~s);

endmodule