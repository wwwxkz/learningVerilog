module fewlog (a, b, c, q);

input a, b, c;
output q;
wire q1, q2, q3, q4;

assign q1 = a & b & c;
assign q2 = ~a & ~b & c;
assign q3 = ~a & b & c;
assign q4 = ~a & b & ~c;

assign q = q1 | q2 | q3 | q4;

endmodule