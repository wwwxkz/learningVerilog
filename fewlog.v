module fewlog ( a , b, q7 );

input wire a, b;
output q7; 
wire q1, q2, q3, q4, q5, q6;

assign q1 = a | b;
assign q2 = b | a;
assign q3 = q1 & q2;

assign q4 = a & b;
assign q5 = b & a;
assign q6 = q4 ^ q5;

assign q7 = q3 | ~b | q6;

endmodule
