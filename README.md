# learningVerilog

Learning Verilog as an effort to start developing Embedded/vxWorks in Rust*, C++, and C. I am expecting Verilog to give me a better foundation of low-level code as bitwise and MEM algorithms. Sure, I could just start codding low-level code right now and improve overtime, but I am with a plenty of time and also willing to get into Nuclear Engineering after graduated in Computer Engineering, so, a good foundation in eletronics and eletric will be handy

I will share my journey learning Verilog here -> 
```
Top - Last
Bottom - Firsts
```

- http://www.asic.co.in/Index_files/tutorials.htm (It just gets better)
- http://www.asic.co.in/projects/vga_files/vga_lcd.htm (This blog is overall insane, but this post, omg, check it out)
- https://github.com/sumukhathrey/Verilog_ASIC_Design (Found these 3 below in my way back home, probably going to work on it monday)
- https://www.csl.cornell.edu/courses/ece5745/handouts/ece5745-tut4-verilog.pdf&ved=2ahUKEwj5pvyd47D5AhWuJrkGHZ8sBpUQFnoECC8QAQ&usg=AOvVaw3uLGfETKCkhDPPxobbifr1
- http://www.asic.co.in/Index_files/verilogexamples.htm
- https://www.researchgate.net/publication/325663297_Lab_2_Digital_logic_circuits_analysis_and_converting_Boolean_expressions_to_digital_circuits (Another cool article)
- https://www.electronics-tutorials.ws/boolean/sum-of-product.html (sounds cool, doesnt it? harderlog.v)
- https://www.ibiblio.org/kuphaldt/electricCircuits/Digital/DIGI_7.html (Cool article, also, it has interesting exercises) 
- https://www.chipverify.com/verilog/verilog-assign-statement
- http://www.ee.surrey.ac.uk/Projects/CAL/digital-logic/gatesfunc/TruthFrameSet.htm (fewlog.v here)
- https://www.cin.ufpe.br/~voo/sd/Aula6#:~:text=Verilog%20%C3%A9%20uma%20linguagem%2C%20como,sistemas%20digitais%20e%20utilizada%20universalmente%3B&text=Hist%C3%B3rico%3A%20Inicialmente%2C%20Verilog%20era%20uma,usada%20para%20modelar%20dispositivos%20ASIC.
- http://gdr.geekhood.net/gdrwpl/verilog.php ( Actually the reasson why I finally started learning Verilog and return 100% focused to my plan of becoming an Embedded/vxWorks developer, thanks GDR! )
